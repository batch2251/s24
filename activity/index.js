const getCube = Math.pow(2, 3);
console.log(`The cube of 2 is ${getCube}`);



const address = ["258 Washington Ave", "NW", "California", "90011" ];
const [address1, city, state, zip] = address
console.log(`I live at ${address1} ${city}, ${state} ${zip}`);





const animal = ["Lolong", "saltwater crocodile", 1075, "20 ft 3 in" ];
const [name, breed, weight, measurement] = animal
console.log (`${name} was a ${breed}. He weighed at ${weight} kgs with a measurement of ${measurement}.`);





const arrayOfNumbers = [1, 2, 3, 4, 5, 15]

arrayOfNumbers.forEach(function (value) {
  console.log(value);
 
});
const reduceNumber = arrayOfNumbers.reduce((total, value) => {
    return total + value;
})  
console.log(reduceNumber);



class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}
const myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);